OMax listens to a musical stream, automatically detects significant features such as pitch, timbre structure, duration etc., builds up a vocabulary of units based on these features, organizes the musical units into higher level formal motivic structures, and finally plays by itself by navigating the structure and generating back  a stylistically coherent stream of music. This results in an on-line smart segmentation and recombination of the input sound material. All these processes happen very fast and simultaneously, so that when used live in real-time interaction with a musician, OMax gives the flavor of an immediate co-improvisation between the performer and the machine. Because the performer also reacts to the machine’s production, thus impacting on the model’s evolution, it may install a double feed-back that we call “stylistic reinjection”. OMax has been performed all around the world with such great performers as Bernard Lubat, [Steve Lehman](http://www.stevelehman.com/bio), [Maria Kimura](http://www.marikimura.com/), [Vijay Iyer](https://vijay-iyer.com/about/), [François Corneloup](http://www.maitemusic.com/Francois-Corneloup-7), [Raphael Imbert](https://www.ircam.fr/person/raphael-imbert-et-benjamin-levy/), [Lori Freedmann](http://lorifreedman.com/en/bio/freedman_lo/Lori_Freedman), [Rohan de Saram](http://www.rohandesaram.co.uk/main.asp?varFunction=Biography), [Mike Svoboda](https://mikesvoboda.net/start.html) (through the HexaGram [Native Alien](http://matralab.hexagram.ca/research/native-alien/) System), Brice Martin, [Mederic Collignon](https://en.wikipedia.org/wiki/M%C3%A9d%C3%A9ric_Collignon), [Cécile Daroux](https://www.dailymotion.com/video/xvptwz), musicians of the Ensemble Intercontemporain, [Denis Beuret](http://www.denisbeuret.ch/), [Mike Garson](http://www.mikegarson.com/about.html), etc. It is one of the more powerful and most used machine improvisation systems today.

Since its inception, OMax has evolved in two directions

- [ImproteK](http://repmus.ircam.fr/nika/improtek) adds the notion of scenario (composition) to the free improvisation scheme of OMax.
- [SoMax](http://repmus.ircam.fr/dyci2/publi_memoires) adds the idea of improvisation interactively guided by the external context (simultaneaous reactive co-improvisation) to the free impro scheme of OMax.

> - Developed by [Musical Representations](https://www.ircam.fr/recherche/equipes-recherche/repmus/) Team
>
> - Training: [Interactions, Improvisations, and the Generative Pro-cess in Composition](https://www.ircam.fr/agenda/interactions-improvisations-et-compositions-de-processus-generatifs/detail/), 
> 30 hours of training, Monday-Friday, April 4-8, 2022 10am-1pm/2:30pm-5:30pm
>
> - [Videos](http://www.dailymotion.com/RepMus) of public performances and concerts using OMax and its siblings
>
> - [The OM Composer's Book](https://www.ircam.fr/recherche/collection-musique-sciences/)

